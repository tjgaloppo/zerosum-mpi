About
=====

This is an MPI implementation of a zero-sum decision solver I developed several years ago.  The algorithm runs in pseudo-polynomial time
and linear space, which gives it a space advantage over the classic pseudo-polynomial time dynamic programming solution.

Problem Statement
=================

Given a set of integers :math:`X={x_1,x_2,...x_n}`, the goal is to determine if there exists a non-empty subset of :math:`X` whose elements sum to zero.

The solution provided here is based on the the idea:

:math:`\hat{x}_k = (\prod_{j=1}^n(1+exp(w*k*x_j)))-1 = \sum_{j=A}^B C_j*exp(w*k*j)`

where A and B are the minimum/maximum possible subset values, respectively (trivially determined by summing all negative/positive values, respectively).
If we let :math:`w=-2\pi i/(B-A+1)`, then :math:`\hat{x}_k` is the k-th component of the forward discrete Fourier transform of :math:`C_A,C_{A+1},...,C_0,...,C_{B-1},C_B`, 
where :math:`C_j` is the count of non-empty subsets with sum :math:`j`.  We can recover :math:`C_0` (the count of zero-sum subsets) then simply by computing the inverse DFT:

:math:`\frac{1}{Z}\sum_{k=0}^{Z-1}\hat{x}_k = \frac{1}{Z}\sum_{k=0}^{Z-1}[\prod_{j=1}^n (1+exp(w*k*x_j)]-1`

requiring only an O(n+log(Z)) bit accumulator to maintain required precision, where :math:`Z=B-A+1`.

For concreteness, here is a simple implementation in Octave:

::

  function c0 = count_zero_sums (x)
    A = sum(x(find(x<0)));
    B = sum(x(find(x>0)));
    Z = B - A + 1;
    w = -2 * pi * i / Z;
    sum = 0;
    for k=0:(Z-1)
      sum = sum + prod(1 + exp(w * k * x)) - 1;
    end
    c0 = round(sum / Z);
  endfunction


Implementation
==============

This implementation uses GNU MPC/MPFR/GMP to handle arbitrarily large problem sizes, and parallelizes the computation of the :math:`\hat{x}_k` values over an MPI cluster.  It also
leverages some mathematical niceties (such as the conjugacy of the DFT components) to minimize the amount of work.  Of course, working with arbitrary precision
numbers is orders of magnitude slower than working with intrinsic types... but if you have enough processors, this code will tackle vary large problems.

This implementation will find a zero sum solution, if any exists, via repetition of the decision problem.

Notes
=====

My excitement when I discovered this method was tempered quickly. Lokshtanov and Nederlof proposed the same idea (in a more generic framework for
converting dynamic programming solutions to algebraic solutions), though they develop their [generalized] solution to polynomial space; while
Daniel Kane gave an alternative linear-space solution.  Please see references for links to these papers.

Author
======

Travis Galoppo (tjg2107 AT columbia.edu)

References
==========

- Kane, Daniel, *Unary Subset-Sum is in Logspace*, http://www-cse.ucsd.edu/~dakane/subsetsum.pdf
- Lokshtanov, D., Nederlof, J., *Saving Space by Algebraization*, http://www.ii.uib.no/~daniello/papers/SaveSpace.pdf
