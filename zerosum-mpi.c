/*******************************************************************
* MPI implementation of zero-sum problem
*
* This code makes a couple assumptions about the MPI
* implementation:
*   1) All processes are provided the command line parameters
*   2) Process 0 has access to stdout
*
* T. Galoppo, 2014
* tjg2107@columbia.edu
*
*******************************************************************/
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>
#include <mpi.h>
#include <gmp.h>
#include <mpfr.h>
#include <mpc.h>

// MPI_BUFFER_SIZE imposes a limit on the size of the problem
// we can solve; should fix this to be dynamic.
#define MPI_BUFFER_SIZE	512

// forward declarations; see functions for descriptions
void mpz_sort(mpz_t* x, int n);
void mpz_max_halfspace(mpz_t, mpz_t*, int);
void add_mpfr_text(void*, void*, int*, MPI_Datatype*);

int main(int argc, char* argv[]){
	char buf[MPI_BUFFER_SIZE], total[MPI_BUFFER_SIZE], *inc;
	int i, k, n, N, rank, size, iteration=0, exists;
	mpz_t *inputs, *x, Z, M, C, S, j;
	mpfr_t tmpfr, tmpfr2, pi;
	
	// init MPI, get our rank and world size
	MPI_Init(&argc, &argv);
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);
	MPI_Comm_size(MPI_COMM_WORLD, &size);	
	
	if(argc < 2){
		if(rank == 0){
			printf("USAGE: %s [integer]+\n", argv[0]);
		}
		MPI_Finalize();
		exit(0);	
	}

	// we need a datatype to pass the partial sums,
	// we use text buffers, which may not be optimal
	MPI_Datatype mpi_mpfr_text;
	MPI_Type_contiguous(MPI_BUFFER_SIZE, MPI_BYTE, &mpi_mpfr_text);
	MPI_Type_commit(&mpi_mpfr_text);

	// define an operator for adding two partial sums;
	// this way we can use collective MPI operations
	// with our defined datatype
	MPI_Op mpi_add_mpfr_text;
	MPI_Op_create(add_mpfr_text, 1, &mpi_add_mpfr_text);
	
	// initialize MPZ variables
	mpz_inits(Z, M, C, S, j, NULL);

	// N is the number of input integers
	N = argc - 1;

	// read the values from the command line
	inputs = malloc(sizeof(mpz_t) * N);
	for(i=0; i<N; i++){
		mpz_init(inputs[i]);
		gmp_sscanf(argv[i+1], "%Zd", inputs[i]);
	}		

	// We need the size of the max possible value for precision determination
	mpz_max_halfspace(Z, inputs, N);
	mpfr_prec_t precision = N + mpz_sizeinbase(Z, 2) + 1;

	// calculate PI to adequate precision
	mpfr_init2(pi, precision);
	mpfr_const_pi(pi, MPFR_RNDN);

	// sort to eliminate max magnitude per iteration; this is 
	// unnecessary, but results in the fastest execution time.
	mpz_sort(inputs, N);

	// the "inc" array indicates which input integers are under evaluation;
	// initially, all of them are
	inc = malloc(N);
	memset(inc, 1, N);

	do {
		// count number of active input integers; if zero, we 
		// can quit.
		n = 0;
		for(i=0; i<N; i++){
			n += inc[i];	
		}
		if(n == 0) break;
		
		// copy active inputs into new array
		x = malloc(sizeof(mpz_t)*n);
		
		int uu = 0;
		for(i=0; i<N; i++){
			if(inc[i]){
				mpz_init(x[uu]);
				mpz_set(x[uu], inputs[i]);
				uu += 1;
			}
		}
	
		// determine max half space sum
		// (max of positive/negative input values) + 1
		// we also make sure this is odd, such that
		// we calculate (Z-1)/2 coefficients
		mpz_max_halfspace(Z, x, n);
		if(mpz_even_p(Z))
			mpz_add_ui(Z, Z, 1);	
	
		// calculate required precision for this round
		precision = n + mpz_sizeinbase(Z, 2) + 1;

		// compute w value, w = -2*pi*i/Z
		mpc_t w, tmp;
		mpc_init2(w, precision);
		mpc_init2(tmp, precision);
		mpc_set_z(tmp, Z, MPC_RNDNN);
		mpc_set_fr(w, pi, MPC_RNDNN);
		mpc_mul_si(w, w, -2.0, MPC_RNDNN);
		mpc_mul_i(w, w, 1, MPC_RNDNN);
		mpc_div(w, w, tmp, MPC_RNDNN);
	
		// we use the property:
		// exp(u * w * x) * exp(w * x) = exp((u+1) * w * x)
		// to avoid exponentiations in the computation loop
		// compute x_hat values, x_hat[i] = exp(w * x[i])
		mpc_t* x_hat = malloc(sizeof(mpc_t) * n);
		for(i=0; i<n; i++){
			mpc_init2(x_hat[i], precision);
			mpc_set_z(tmp, x[i], MPC_RNDNN);
			mpc_mul(tmp, tmp, w, MPC_RNDNN);
			mpc_exp(x_hat[i], tmp, MPC_RNDNN);
		}

		// compute number of iterations, M=(Z-1)/2
		// (ie, how many total coefficients need to be computed?)
		mpz_sub_ui(M, Z, 1);
		mpz_div_ui(M, M, 2);

		// compute iterations per node, C=M/size
		// (ie, how many coefficients should this node compute?)
		mpz_div_ui(C, M, size);
	
		// compute starting point for this node, S=C*rank+1
		mpz_mul_ui(S, C, rank); 
		mpz_add_ui(S, S, 1); 		
			
		// if this is the last node, then our count may be slightly off
		// C = M - S + 1
		if(rank == size-1){			
			mpz_sub(C, M, S);	
			mpz_add_ui(C,C,1);			
		}	
	
		// compute the x_bar values for this starting position
		// x_bar[i] = x_hat[i] ^ S = exp(w * x[i])^S = exp(S * w * x[i])
		mpc_t* x_bar = malloc(sizeof(mpc_t)*n);
		for(i=0; i<n; i++){
			mpc_init2(x_bar[i], precision);
			mpc_pow_z(x_bar[i], x_hat[i], S, MPC_RNDNN);
		}
	
		// prepare to compute coefficients for this node
		mpc_t product, sum;
		mpc_init2(product, precision);
		mpc_init2(sum, precision);
	
		// sum = 0
		mpc_set_ui(sum, 0, MPC_RNDNN);
	
		// calculate coefficients
		mpz_set_ui(j, 0); // j = 0
		while( mpz_cmp(j, C) < 0 ){
			// product = (1+x_bar[0]) = (1 + exp((S+j) * w * x[0]))
			mpc_add_ui(product, x_bar[0], 1, MPC_RNDNN);
			
			// update for next iteration:
			// x_bar[0] = x_bar[0] * x_hat[0] 
			//					= exp((S+j)*w*x[0]) * exp(w*x[0])
			//					= exp((S+j+1) * w * x[0])
			mpc_mul(x_bar[0], x_bar[0], x_hat[0], MPC_RNDNN);
		
			for(k=1; k<n; k++){
				mpc_add_ui(tmp, x_bar[k], 1, MPC_RNDNN);
				// product = product * (1 + x_bar[k])
				//				 = product * (1 + exp((S+j) * w * x[k]))
				mpc_mul(product, product, tmp, MPC_RNDNN);
				// xbar[k] = exp((S+j+1) * w * x[k])
				mpc_mul(x_bar[k], x_bar[k], x_hat[k], MPC_RNDNN);
			}
		
			// sum = sum + product
			mpc_add(sum, sum, product, MPC_RNDNN);
		
			// j = j + 1
			mpz_add_ui(j, j, 1);	
		}
		
		// convert Re(sum) to base 10 text string
		memset(buf, 0, MPI_BUFFER_SIZE);
		mpfr_snprintf(buf, MPI_BUFFER_SIZE-1, "%Re", mpc_realref(sum));
		
		// Collective reduction; total is final count of zero subsets, as text string
		// If we were solving only the decision problem, we could just use MPI_Reduce()
		MPI_Allreduce(buf, total, 1, mpi_mpfr_text, mpi_add_mpfr_text, MPI_COMM_WORLD);

		mpfr_init2(tmpfr, precision);
		mpfr_init2(tmpfr2, precision);
		
		// convert text string back to MPFR
		mpfr_set_str(tmpfr, total, 10, MPFR_RNDN);
		
		// we only calculated half of the coefficients, but the other half
		// are the conjugate transposes, so we can double it
		mpfr_mul_ui(tmpfr, tmpfr, 2, MPFR_RNDN);
		
		// sum = (2^n + sum - Z) / Z
		// so, Z coefficients were calculated, each need to have
		// 1 subtracted from them, then take the average.
		// the first coefficient is always 2^n-1 (ie, e^0 for all n inputs)
		mpfr_ui_pow_ui(tmpfr2, 2, n, MPFR_RNDN);
		mpfr_add(tmpfr, tmpfr, tmpfr2, MPFR_RNDN);
		mpfr_sub_z(tmpfr, tmpfr, Z, MPFR_RNDN);
		mpfr_div_z(tmpfr, tmpfr, Z, MPFR_RNDN);
		mpfr_round(tmpfr, tmpfr);
		
		if(rank == 0)
			mpfr_printf("iteration %d, count=%.0Rf\n", iteration, tmpfr);
		
		// if tmpfr > 0 then zero subset exists in x[]
		exists = mpfr_cmp_ui(tmpfr, 0) > 0;
		
		mpfr_clears(tmpfr, tmpfr2, NULL);
		
		// if no zero subset exists in x[], then we need
		// the withheld input value.
		if(!exists){
			if(iteration == 0){
				if(rank == 0) printf("No zero subset in inputs\n");
				memset(inc, 0, n);
			} else {
				inc[iteration-1] = 1;	
			}	
		}
	
		// withhold value for next iteration
		if(iteration < N)
			inc[iteration] = 0;						
		
		// clear out the x[i]'s and x_hat[i]'s and deallocate the arrays
		for(i=0; i<n; i++){
			mpz_clear(x[i]);
			mpc_clear(x_hat[i]);
			mpc_clear(x_bar[i]);
		}
		free(x_bar);
		free(x_hat);
		free(x);	

		mpc_clear(product);
		mpc_clear(sum);
		mpc_clear(w);
		mpc_clear(tmp);
		
		iteration += 1;
	} while(iteration <= N);
	
	// cleanup
	mpz_clears(Z, M, C, S, j, NULL);

	MPI_Op_free(&mpi_add_mpfr_text);
	MPI_Type_free(&mpi_mpfr_text);

	MPI_Finalize();
	
	// output the final list, and clean up
	for(i=0; i<N; i++){
		if(rank == 0 && inc[i]){
			 mpz_out_str(NULL, 10, inputs[i]); 
			 printf(" ");
		}
		mpz_clear(inputs[i]);	
	}
	if(rank == 0) printf("\n");
	
	free(inc);
	free(inputs);
	
	mpfr_clear(pi);
	mpfr_free_cache();
	
	return 0;
}

// add two text encoded MPFR values
void add_mpfr_text(void* in, void* inout, int* len, MPI_Datatype *dataType){
	mpfr_t a, b;
	mpfr_init2(a, 2*MPI_BUFFER_SIZE);
	mpfr_init2(b, 2*MPI_BUFFER_SIZE);
	mpfr_set_str(a, in, 10, MPFR_RNDN);
	mpfr_set_str(b, inout, 10, MPFR_RNDN);
	mpfr_add(a,a,b,MPFR_RNDN);
	mpfr_snprintf(inout, MPI_BUFFER_SIZE-1, "%Re", a);
	mpfr_clears(a,b,NULL);
}

// determine max absolute sum of positive/negative halfspaces
void mpz_max_halfspace(mpz_t rop, mpz_t* x, int n){
	int i;
	mpz_t A,B;
	mpz_inits(A,B,NULL);
	mpz_set_ui(A,0);
	mpz_set_ui(B,0);
	for(i=0; i<n; i++){
		if(mpz_cmp_ui(x[i], 0) > 0){
			mpz_add(B, B, x[i]);
		} else {
			mpz_sub(A, A, x[i]);
		}	
	}
	if(mpz_cmp(A,B) > 0)
		mpz_set(rop, A);
	else
		mpz_set(rop, B);
	mpz_add_ui(rop, rop, 1);
	mpz_clears(A,B,NULL);
}

// mpz selection sort
// sort by absolute value, descending
void mpz_sort(mpz_t* x, int n){
	int i,j;
	mpz_t a, b;
	mpz_inits(a,b,NULL);
	for(i=0; i<n-1; i++){
		for(j=i+1; j<n; j++){
			mpz_abs(a, x[i]);
			mpz_abs(b, x[j]);
			if(mpz_cmp(a,b) < 0){
				mpz_swap(x[i], x[j]);
			}
		}
	}
	mpz_clears(a,b,NULL);	
}
